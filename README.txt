Nodewords meta title tag
========================
This module is a really simple extension to Nodewords: D6 Meta Tags which makes
it possible to add <meta name="title" ... /> tags to pages.

Requirements
============
Nodewords 6.x-1.13

Installation
============
[0. Download, install & enable Nodewords; see its README.txt for full
instructions.]

1. Install & enable the module as usual; see http://drupal.org/node/70151 for
   further information.

2. Admin makes a permission available that allows only properly
   permissioned users to make use of the admin toolbar. Users with the
   'use admin toolbar' permission will be able to use the toolbar.

Contributors
============
- scorchio (Zoltán Adamek)
